﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('landing', landing);

    landing.$inject = ['$location', '$rootScope', '$timeout', 'deviceService', 'modalService'];

    function landing($location, $rootScope, $timeout, deviceService, modalService) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'landing';
        activate();
        function activate() {
            deviceService.defaultBackButtonHandler = onDeviceBackButton;
        }

        function onDeviceBackButton(args) {
            $timeout(function () {
                modalService.showModal(function (option) {
                    if (option == modalService.YES) {
                        deviceService.exitApplication();
                    }
                },
                {
                    buttons: modalService.YES | modalService.NO,
                    message: 'Do you want to quit the application?',
                    title: 'Close Application'
                });
            }, 1);
        }
    }
})();
