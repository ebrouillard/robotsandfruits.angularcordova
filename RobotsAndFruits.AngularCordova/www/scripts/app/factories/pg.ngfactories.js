﻿(function () {
    'use strict';

    var app = angular.module('app');
    /**
     * Device local storage service
     */
    app.factory('localStorageService', storageService);
    storageService.$inject = ['$http'];
    function storageService($http) {
        var service = {
            // Constants
            NO_LOCAL_STORAGE: 'NO_LOCAL_STORAGE'
        };
        /**
         * Gets localstorage is supported by device
         */
        Object.defineProperty(service, 'hasLocalStorage', {
            get: function () {
                if (typeof (Storage) !== "undefined") {
                    return true;
                } else {
                    return false;
                }
            }
        });
        /**
         * Asserts if localstorage is available and throws if not
         */
        function assertLocalStorage() {
            if (!service.hasLocalStorage)
                throw service.NO_LOCAL_STORAGE;
        }
        service.assertLocalStorage = assertLocalStorage;
        /**
         * Sets item in localstorage
         */
        function setItem(key, value) {
            service.assertLocalStorage();
            localStorage.setItem(key, value);
        }
        service.setItem = setItem;
        /**
         * Gets item in localstorage
         */
        function getItem(key) {
            service.assertLocalStorage();
            return localStorage.getItem(key);
        }
        service.getItem = getItem;
        /**
         * Creates localstorage backed property on target
         */
        function backProperty(target, key, value) {
            service.assertLocalStorage();
            Object.defineProperty(target, key, {
                get: function () {
                    return service.getItem(key);
                },
                set: function (set) {
                    service.setItem(key, set);
                }
            });
            if (value !== 'undefined' && !target[key])
                target[key] = value;
        }
        service.backProperty = backProperty;
        return service;
    };
    /**
     * Device service factory
     */
    app.factory('deviceService', deviceService);
    deviceService.$inject = ['$http', '$location', '$rootScope', 'localStorageService'];
    function deviceService($http, $location, $rootScope, localStorageService) {

        var service = {
            // Constants
            GEOPOSITION_NOT_ENABLED: "GEOPOSITION_NOT_ENABLED",
            onDeviceBackButton: function (args) { $rootScope.$broadcast('onDeviceBackButton', args); },
            defaultBackButtonHandler: undefined
        };
        /**
         * Gets is a Windows Universal App value
         */
        Object.defineProperty(service, "isWindowsUniversal", {
            get: function () {
                return window.WinJS && window.WinJS.Application;
            }
        });
        // Create local storage service backed properties for device service
        localStorageService.backProperty(service, "geoLocationEnabled", false);

        // #### HOOK LISTENERS
        // Hook device buttons
        addDeviceButtonsListeners();

        // ##### PUBLIC METHODS #######
        /**
         * Requests a position and returns through callbacks
         */
        function getPosition(onSuccess, onError, options) {
            if (service.geoLocationEnabled === 'false') {
                onError(Service.GEOPOSITION_NOT_ENABLED);
            }
            navigator.geolocation.getCurrentPosition(onSuccess, onError, options);
        }
        service.getPosition = getPosition;
        /**
         * Instantiates a position watcher and returns through callbacks
         */
        function watchPosition(onSuccess, onError, options) {
            if (service.geoLocationEnabled === 'false') {
                onError(Service.GEOPOSITION_NOT_ENABLED);
            }
            navigator.geolocation.watchPosition(onSuccess, onError, options);
        }
        service.getPosition = getPosition;
        /**
         * Toggles geo location enabled 
         */
        function toggleGeoLocationEnabled() {
            var isDisabled = service.geoLocationEnabled === 'false';
            service.geoLocationEnabled = isDisabled;
            if (isDisabled) {
                service.getPosition(function (position) {
                    console.log(position);
                    service.geoLocationEnabled = true;
                }, function (error) {
                    console.error(error);
                    service.geoLocationEnabled = false;
                    $rootScope.$digest();
                });
            }
        }
        service.toggleGeoLocationEnabled = toggleGeoLocationEnabled;
        /**
         * Exits the application
         */
        function exitApplication() {
            if (navigator.app && navigator.app.exitApp) navigator.app.exitApp();
            else if (self) self.close();
        }
        service.exitApplication = exitApplication;
        // ##### PUBLIC METHODS #######

        return service;

        // #### PRIVATE METHODS #######
        /**
         * Registers device buttons
         */
        function addDeviceButtonsListeners() {
            if (service.isWindowsUniversal)
                window.WinJS.Application.addEventListener('backclick', backbuttonCallback, false);
            else
                document.addEventListener('backbutton', backbuttonCallback, false);
        }
        /**
         * Back button event callback
         */
        function backbuttonCallback(e) {
            if (!service.defaultBackButtonHandler || !service.defaultBackButtonHandler()) {
                if (e.preventDefault)
                    e.preventDefault();
                service.onDeviceBackButton();
                return true;
            }
            return false;
        }
        // ##### PRIVATE METHODS #######
    }
    /**
    * Modal service
    */
    app.factory('modalService', modalService);
    modalService.$inject = ['$rootScope', '$timeout', 'deviceService'];
    function modalService($rootScope, $timeout, deviceService) {
        var _backButtonHandler = undefined;
        var _visible = false;
        var _ok = false;
        var _cancel = false;
        var _yes = false;
        var _no = false;
        var _message = false;
        var _title = false;

        var service = {
            OK: 1,
            CANCEL: 2,
            YES: 4,
            NO: 8
        };

        Object.defineProperty(service, 'visible', {
            get: function () { return _visible; },
            set: function (value) { _visible = value; }
        });
        Object.defineProperty(service, 'ok', {
            get: function () { return _ok; },
            set: function (value) { _ok = value; }
        });
        Object.defineProperty(service, 'cancel', {
            get: function () { return _cancel; },
            set: function (value) { _cancel = value; }
        });
        Object.defineProperty(service, 'yes', {
            get: function () { return _yes; },
            set: function (value) { _yes = value; }
        });
        Object.defineProperty(service, 'no', {
            get: function () { return _no; },
            set: function (value) { _no = value; }
        });
        Object.defineProperty(service, 'message', {
            get: function () { return _message; },
            set: function (value) { _message = value; }
        });
        Object.defineProperty(service, 'title', {
            get: function () { return _title; },
            set: function (value) { _title = value; }
        });
        /**
         * Shows modal
         */
        function showModal(okCallback, options) {
            if (!service.visible) {
                /**
                 * Suspend back button
                 */
                _backButtonHandler = deviceService.defaultBackButtonHandler;
                if (options.defaultOption)
                    deviceService.defaultBackButtonHandler = closeModalDefault;
                else
                    deviceService.defaultBackButtonHandler = undefined;
                /**
                 * Configure
                 */
                service.ok = ((service.OK & options.buttons) == service.OK);
                service.cancel = ((service.CANCEL & options.buttons) == service.CANCEL);
                service.yes = ((service.YES & options.buttons) == service.YES);
                service.no = ((service.NO & options.buttons) == service.NO);
                service.message = options.message;
                service.title = options.title;
                service.visible = true;
                service.callback = okCallback;
                service.defaultOption = options.defaultOption;
            }
        };
        function closeModalDefault()
        {
            if (service.defaultOption)
                $timeout(closeModal(service.defaultOption), 1);
        }
        /**
         * Closes modal
         */
        function closeModal(option) {
            service.visible = false;
            /**
             * Reset backbutton handler
             */
            deviceService.defaultBackButtonHandler = _backButtonHandler;
            service.callback(option);
            service.callback = undefined;
        }
        service.showModal = showModal;
        service.closeModal = closeModal;
        return service;
    }
})();