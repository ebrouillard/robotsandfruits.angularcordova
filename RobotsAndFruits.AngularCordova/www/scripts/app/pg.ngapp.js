﻿(function () {
    'use strict';

    var app = angular.module('app', [
        // Angular modules 
        'ngAnimate',
        'ngRoute',
        // Custom modules 
        // 3rd Party Modules
    ]);
    app.config(function ($provide) {
        $provide.decorator("$exceptionHandler", function ($delegate, $injector) {
            return function (exception, cause) {
                var modalService = $injector.get("modalService");
                modalService.showModal(function () { }, {
                    buttons: modalService.OK,
                    message: 'An error occured: ' + exception,
                    title: 'Error'
                });
                $delegate(exception, cause);
            };
        });
    });
})();