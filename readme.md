# RobotsAndFruits.AngularCordova
## A simple Cordova/Angular Bootstrap

In a VS2015 TACO new project, this package bootstraps AngularJS quickly and adds helper directives and factories/services

### Provides: 
- Initial layout 
- Toolbar directive 
- SnapJS integration
- Device agnostic services

### Based on: 
- Angular 1.4.7 
- Bootstrap 3.3.5 
- JQuery 2.1.4 
- SnapJS 

### How To Use

This section describes how to use the bootstrap in your Visual Studio 2015 Apache Cordova Application:
This package **replaces** files in the **www** project folder:

#### Setup:
1. Create a new Apache Cordova Applcation Project in VisualStudio 2015 (you should backup index.js. index.css and index.html)
2. PM> Install-Package VS2015.TACO.AngularOnCordova.Bootstrap  

### Pull Requests

Don't hesitate to submit pull requests to the owner